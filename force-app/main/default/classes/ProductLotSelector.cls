/**
 * @File Name          : ProductLotSelector.cls
 * @Description        : 
 * @Author             : Jon Astemborski
 * @Group              : 
 * @Last Modified By   : Jon Astemborski
 * @Last Modified On   : 2/20/2020, 4:42:50 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/20/2020   Jon Astemborski     Initial Version
**/
public with sharing class ProductLotSelector  extends fflib_SObjectSelector{
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Product_Lot__c.Id, 
            Product_Lot__c.Product__c,
            Product_Lot__c.Expiration_Date__c,
            Product_Lot__c.Lot_Number__c,
            Product_Lot__c.Manufactured_Date__c};
    }
    public Schema.SObjectType getSObjectType() {
        return Product_Lot__c.sObjectType;
    }
    public List<Product_Lot__c> selectByLotNumber(String lotNumber) {
        return (List<Product_Lot__c>) Database.query(
        newQueryFactory().
        selectField('Product__r.Name').
        setCondition('Lot_Number__c = :lotNumber').
        toSOQL());
    }

}
