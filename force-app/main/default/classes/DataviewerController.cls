/**
 * @File Name          : DataviewerController.cls
 * @Description        : 
 * @Author             : Jon Astemborski
 * @Group              : 
 * @Last Modified By   : Jon Astemborski
 * @Last Modified On   : 2/20/2020, 6:18:53 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    2/20/2020   Jon Astemborski     Initial Version
**/
public without sharing class DataviewerController {
    public DataviewerController() {

    }

    public class ProductInformation{
        @AuraEnabled
        public String name {get;set;}
        @AuraEnabled
        public Date expirationDate {get;set;}
        @AuraEnabled
        public Date manufacturingDate {get;set;}
        @AuraEnabled
        public String lotNumber {get;set;}
        @AuraEnabled
        public String checkbox {get;set;}
        @AuraEnabled
        public String relatedQe {get;set;}
        @AuraEnabled
        public String productId {get;set;}

        public ProductInformation(){}

        public ProductInformation(String name, Date expirationDate, Date manufacturingDate, String lotNumber, String productId){
            this.name = name;
            this.expirationDate = expirationDate;
            this.manufacturingDate = manufacturingDate;
            this.lotNumber = lotNumber;
            this.productId =productId;
            this.checkbox = '';
            this.relatedQe ='';
        }
    }

    @AuraEnabled
    public static Boolean updateQualityEvent(DataviewerController.ProductInformation prodInformation){
        try { 
            CMPL123QMS__Deviation__c qe = [SELECT Id, Product_Lot_Number__c,  TWD_Product__c, Lot_Manufactured_Date__c, Lot_Expiration_Date__c FROM CMPL123QMS__Deviation__c WHERE Id =: prodInformation.relatedQe];
            qe.Product_Lot_Number__c=prodInformation.lotNumber;
            qe.TWD_Product__c = prodInformation.productId;
            qe.Lot_Manufactured_Date__c=Date.valueOf(prodInformation.manufacturingDate); 
            qe.Lot_Expiration_Date__c=Date.valueOf(prodInformation.expirationDate);
            update qe;
            return true;
        }
        catch (DmlException DmlException) {
            System.debug(DmlException);
            return false;
        }
    }

    @AuraEnabled
    public static String retrieveProductInformation(String lotNumber){
        List <Product_Lot__c> productLots = new ProductLotSelector().selectByLotNumber(lotNumber);
        List<DataviewerController.ProductInformation> parsedProductInformation = new List<DataviewerController.ProductInformation>();
        for ( Product_Lot__c productLot : productLots ){
            parsedProductInformation.add(new DataviewerController.ProductInformation(productLot.Product__r.Name, productLot.Expiration_Date__c,
                        productLot.Manufactured_Date__c, productLot.Lot_Number__c, productLot.Product__c));
        }
        return JSON.serialize(parsedProductInformation);
    }
}
