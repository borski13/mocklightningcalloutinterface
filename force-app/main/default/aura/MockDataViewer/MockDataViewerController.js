({
    init: function (component, event) {
        component.set("v.HideSpinner", false);
    },
    scriptsLoaded : function(component, event, helper) {
        console.log('Script loaded..'); 
        helper.initDataTable();
    },
    cancel: function(component, event, helper){
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire(); 
    },
    save: function(component, event, helper){
        try{
        component.set("v.HideSpinner", true);

        var action = component.get("c.updateQualityEvent");
        var prodInfo = component.get("v.selectedProductInformation");
        prodInfo.relatedQe = component.get("v.recordId");
        action.setParams({'prodInformation': component.get("v.selectedProductInformation")});
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                // let result = response.getReturnValue();
                component.set("v.HideSpinner", true);
                $A.get("e.force:closeQuickAction").fire();  
                let navService = component.find("navService");
                let pageReference = {
                    type: 'standard__recordPage',
                    attributes: {
                        "recordId": component.get("v.recordId"),
                        "objectApiName": component.get("v.sobjecttype"),
                        "actionName": "view"
                    }
                };
                navService.navigate(pageReference);
            }
            else {
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message)
            }
            component.set("v.HideSpinner", false);
        });
        // Send action off to be executed
        $A.enqueueAction(action);
        }
        catch (error){
            console.log(error);
        }
    },
    search: function(component, event, helper){
        var table = $('#productTable').DataTable().off('select')
        .off( 'deselect');
        console.log("Searching")
        component.set("v.selectedProductInformation", {});
        var action = component.get("c.retrieveProductInformation");
        action.setParams({'lotNumber': component.get("v.lotNumber")});
        component.set("v.HideSpinner", true);
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let result = response.getReturnValue();
                var arr = helper.buildDataSet(JSON.parse(result));
                var table = helper.buildDataTable(arr);
                table
                .on( 'select', function ( e, dt, type, indexes ) {
                    var rowData = table.rows( indexes ).data().toArray();
                    component.set("v.selectedProductInformation", rowData[0]);
                    console.log("selected: " + JSON.stringify(rowData[0]))
                    component.set("v.disableSave", false);
                } )
                .on( 'deselect', function ( e, dt, type, indexes ) {
                    var rowData = table.rows( indexes ).data().toArray();
                    component.set("v.selectedProductInformation", {});
                    console.log("deselected: " + JSON.stringify(rowData[0]))
                    component.set("v.disableSave", true);
                } );
            }
            else {
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message)
            }
            component.set("v.HideSpinner", false);
        });
        // Send action off to be executed
        $A.enqueueAction(action);
    }
})
 