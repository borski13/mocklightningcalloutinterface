({
    buildDataTable : function(data) {
        var table = $('#productTable').DataTable({
            "destroy":true,
            "data": data,
            "columnDefs": [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            "select": {
                style:    'os',
                selector: 'td:first-child'
            },
            "order": [[ 1, 'asc' ]],
            "columns": [
                {"data": "checkbox", 
                  "title": ""
                },
                {
                    "data": "lotNumber", 
                    "title": "Lot Number"
                },
                {
                "data": "name", 
                "title": "Product"
            }, 
            {
                "data": "manufacturingDate", 
                "title": "Manufacturing Date"
            },
            {
                "data": "expirationDate", 
                "title": "Expiration Date"
            }]
        })
        return table;
    },
    initDataTable:function(){ 
        var table =  $('#productTable').DataTable({
            "columnDefs": [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            "select": {
                style:    'os',
                selector: 'td:first-child'
            },
            "order": [[ 1, 'asc' ]],
            "columns": [
                {"data": "checkbox", 
                  "title": ""
                },
                {
                    "data": "lotNumber", 
                    "title": "Lot Number"
                },
                {
                "data": "name", 
                "title": "Product"
            }, 
            {
                "data": "manufacturingDate", 
                "title": "Manufacturing Date"
            },
            {
                "data": "expirationDate", 
                "title": "Expiration Date"
            }]
        })
    },
    buildDataSet: function(data){
        let arrayOfArray = [];
        for ( var val in data ){
            let arr = []
            arr = data[val];
            arrayOfArray.push(arr);
        }
        return arrayOfArray;
    }
})
